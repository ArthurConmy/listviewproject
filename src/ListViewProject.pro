# QT -= gui
QT += qml gui quick ## I tinkered with these a little D:
                    ## all on one line - code review

CONFIG += c++11 ## console
## CONFIG -= app_bundle ## commented out - code review

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    listmodeltwo.h \
    timerclass.h

SOURCES += \
        listmodeltwo.cpp \
        main.cpp \
        timerclass.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ListViewOne.qml
    ListViewDelegateOne.qml
    Pusher.qml ## ooh, the project file was fine without this, but I've just added it anyway
               ## code review: this is not really necessary but no harm in leaving in
RESOURCES += \
    qml.qrc
