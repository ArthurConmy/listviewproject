#include "timerclass.h"

#include <QObject>
#include <QDateTime>

// all of this was copy+pasted

namespace Leapian {
TimerClass::TimerClass(QObject *parent)
{
    Q_UNUSED(parent);
}

QString Leapian::TimerClass::currentTime()
{
    QString time = QDateTime::currentDateTime().toString();
    //qDebug() << time;

    return time;
}

void Leapian::TimerClass::slotTimeChanged()
{
    currentTime();
    emit timeChanged();
    //qWarning() << " Timer Fired";
}
}
