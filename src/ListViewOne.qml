import QtQuick 2.12

Rectangle {

    width: 500
    height: 500 // ideally these would be based on the parent (window's) values
                // but this was bugging for me

    color: "grey"
    border.color: "black"
    border.width: width*0.01

    ListView {
        id: listViewOne

        anchors.fill: parent
        anchors.margins: parent.height*0.2 // think that this was creating a 'binding loop' when I didn't include the parent

        model: simpleListModel // not with { }!

        delegate: ListViewDelegateOne {

            firstNameText: firstName
            deathText: death
            surnmameText: secondName

           // I think that the ListView makes this all
           // spaced out nicely, no need for anchoring
        }

        spacing: 20
    }

    Pusher {

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 20

        onClicked: {
            simpleListModel.createNewItem("M" + simpleListModel.rowCount().toString());
            // this gives identifiers M1, M2, ...
            console.log("M"+simpleListModel.rowCount().toString()); // log the ID just added
                                                                    // very useful in testing
        }
    }
}
