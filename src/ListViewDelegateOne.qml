import QtQuick 2.0

// later this could be turned back into a component
// (for now it's a rectangle)

Rectangle {
    id: rect

    height: 100
    width: 300
    color: "lightsteelblue" // this is our background

    property alias firstNameText:  tlText.text // I *think* that this aliasing is necessary
    property alias deathText: midText.text
    property alias surnmameText: trText.text

    Text {
        id: tlText

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10

        text: "" // will be assigned in listViewOne.qml
    }

    Text {
        id: midText

        anchors.centerIn: parent

        font.pixelSize: 0.8*parent.height // this makes the year big

        text: ""
    }

    Text {
        id: trText

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 10

        text: ""
    }
}
