#include "listmodeltwo.h"
#include <QFile>

namespace Leapian {
namespace CustomListModel {

ListModelTwo::ListModelTwo(const QString &themodelUuid, QObject *parent)
    : QAbstractListModel (parent)
{

    // this constructor initialises the list model with some sample dead people

    this->m_roleNames[deadIDRole] = "ID";
    this->m_roleNames[firstNameRole] = "firstName";
    this->m_roleNames[secondNameRole] = "secondName";
    this->m_roleNames[yearofDeathRole] = "death";

    this->modelUuid = themodelUuid;

    DeadPerson* BR;
    BR = new DeadPerson;
    BR->deadUuid = "M1";
    BR->firstName = "Bob";
    BR->secondName = "Ross";
    BR->yearOfDeath = 1995;
    this->model.append(BR);

    DeadPerson* CFG;
    CFG = new DeadPerson;
    CFG->deadUuid = "M3";
    CFG->firstName = "Carl Friedrich";
    CFG->secondName = "Gauss";
    CFG->yearOfDeath = 1855;
    this->model.append(CFG);

    DeadPerson* BR2;
    BR2 = new DeadPerson;
    BR2->deadUuid = "M4";
    BR2->firstName = "Bernhard";
    BR2->secondName = "Riemann";
    BR2->yearOfDeath = 1866;
    this->model.append(BR2);

    DeadPerson* DH;
    DH = new DeadPerson;
    DH->deadUuid = "M5";
    DH->firstName = "David";
    DH->secondName = "Hilbert";
    DH->yearOfDeath = 1943;
    this->model.append(DH);

    DeadPerson* HP;
    HP = new DeadPerson;
    HP->deadUuid = "M6";
    HP->firstName = "Henri";
    HP->secondName = "Poincare";
    HP->yearOfDeath = 1912;
    this->model.append(HP);

    DeadPerson* LE;
    LE = new DeadPerson;
    LE->deadUuid = "M7";
    LE->firstName = "Leonhard";
    LE->secondName = "Euler";
    LE->yearOfDeath = 1783;
    this->model.append(LE);
}

ListModelTwo::~ListModelTwo()
{
    //// on the class object destruction when the destructor is called then call the clear model ducntion
    this->clearModel();
}

int ListModelTwo::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->model.count();
}

//// for each value we have in our Item we need to create a case that assigns that value
QVariant ListModelTwo::data(const QModelIndex &index, int role) const
{
    // most of the functions here are straightforward modifications of the boilerplate

    QVariant value;
    if(this->indexValid(index)) {
        DeadPerson *deadPerson = this->model[index.row()];
        if(deadPerson) { // seems hacky to me; we're saying 'not nullptr' I think
            switch (role) {
            case deadIDRole: {
                value = deadPerson->deadUuid;
                break;
            }

            case firstNameRole: {
                value = deadPerson->firstName;
                break;
            }

            case secondNameRole: {
                value = deadPerson->secondName;
                break;
            }

            case yearofDeathRole: {
                value = deadPerson->yearOfDeath;
                break;
            }
            }
        }
    }

    return value;
}

//// To add a new item in the model first thing that needs to be called is the beginInsertRows(..)
//// Then a new pointer to the item  is being created
//// The item's values are being assigned.

void ListModelTwo::createNewItem(const QString& newID) // note that we generate the name and year death in this function.
                                                       // for now there's no functionality to pass these from the QML
{
    QList<QString> validFirstNames = {"Phil", "Gary", "James", "Rudolf"};
    QList<QString> validSecondNames = {"Smith", "Hunter", "Henderson", "Austin", "Harrison", "Jamieson"};

    // some lists of a few common first and second names

    DeadPerson* newDeadPerson;
    newDeadPerson = new DeadPerson;
    newDeadPerson->deadUuid = newID;

    newDeadPerson->firstName = validFirstNames[(int)(rand() % (int)validFirstNames.size())];
    newDeadPerson->secondName = validSecondNames[(int)(rand() % (int)validSecondNames.size())];

    // the use of rand() is such that the first and second name are uniformly random names
    // in each of the two above QList<QString>s

    newDeadPerson->yearOfDeath = 1000 + rand() % 1020; // generate a random year

    // notably I had a bug where pushing the button did nothing;
    // what had gone wrong is that I hadn't added the emits here
    emit beginInsertRows(QModelIndex(), this->model.length(), this->model.length()); // I *think* that these values are correct
    this->model.append(newDeadPerson);
    emit endInsertRows();
}

// there were a bunch of functions from the boilerplate in this space here
// (I didn't implement them)

void ListModelTwo::clearModel()
{
    // copy+pasted this function

    while((this->model).count()) {
       this->beginRemoveRows(QModelIndex(), 0, 0);
        DeadPerson *t = this->model.takeAt(0);
        delete t; //// or t->deleteLater() if Item inherits from QObject
        t = nullptr;
        this->endRemoveRows();
    }
}

//// we are assigning a string for each of the roles.  That way we can access the values  with that string from QML
QHash<int, QByteArray> ListModelTwo::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[deadIDRole] = "deadUuid";
    roles[firstNameRole] = "firstName";
    roles[secondNameRole] = "secondName";
    roles[yearofDeathRole] = "death";
    //// add more as needed
    // ^done exactly that
    return roles;
}

// some more non-implemented functions were here

bool ListModelTwo::indexValid(const QModelIndex &index) const
{
    // copy+pasted
    return (index.row() >= 0 && index.row() < this->model.count());
}

void ListModelTwo::notifyDataChanged(int index)
{
    QModelIndex idx = this->index(index);
    emit this->dataChanged(idx, idx);
}

} //// end namespace CustomListModel
} //// end namespace Leapian
