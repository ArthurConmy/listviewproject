#ifndef TIMERCLASS_H
#define TIMERCLASS_H

#include <QObject>

// this is all almost verbatim copty+pasted

namespace Leapian {

class TimerClass: public QObject
{
    Q_OBJECT
//// QProperty to expose the currentTime to the QML without using a Q_INVOKABLE function
    Q_PROPERTY(QString currentTime READ currentTime NOTIFY timeChanged)
public:
    explicit TimerClass(QObject *parent = nullptr);

    QString currentTime();
signals:
    void timeChanged();

public slots:
//// slot that is fired on time changed
    void slotTimeChanged();
};

} // namespace Leapian


#endif // TIMERCLASS_H
