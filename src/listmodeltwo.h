#ifndef LISTMODELTWO_H
#define LISTMODELTWO_H

#include <QAbstractListModel>

//// Two namespaces

namespace Leapian {
namespace CustomListModel {

struct DeadPerson {
    QString deadUuid; // This doesn't do anything in my program, but
                      // it was mentioned that having an ID could
                      // sometimes be useful
    QString firstName;
    QString secondName;
    int yearOfDeath; // all these members are pretty intuitive
};

//// Subclassing QAbstractListModel
class ListModelTwo : public QAbstractListModel
{
//// The Q_OBJECT macro transforms this class to use Q_OBJECTS
     Q_OBJECT
public:
    //// Notion notes:
    //// In our case the modelUuid & the modelDataPath are not needed so feel free to omit them
    //// modeulUuid is used if we want an identifier for this specific model; useful when we have many instances of this class
    //// modelDataPath is used if we want to load/unload the contents from a file, omit otherwise
    //// Constructor will need to contain a pointer to the app passed from main

    ListModelTwo(const QString &modelUuid = "", QObject *parent = nullptr); // the cadaques added an explicit in the constructor

    //// Destructor: used for clearing the internal pointers
    ~ListModelTwo();

    //// To be able to expose the values in the qml roles are assigned in an enumeration
    //// Enumberations must always be written with the first letter of each word capitalised
    enum ItemRoles {
        deadIDRole = Qt::UserRole + 1, // only this first role is required (see code review)
        firstNameRole,
        secondNameRole,
        yearofDeathRole
    }; // I'm unsure of what exactly 'roles' are (members that QML can access?!) but happy copy+pasting boilerplate code and adjusting

    ////The rowCount & data functions are reimplemented from QAbstractListModel

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    //// This is how you set a function to be a Q_INVOKABLE and be abled to be called from QML
    Q_INVOKABLE void createNewItem(const QString& newID);
    // this solves the task of dynamic population

    //// example of an update function, ideally have one for each field
//  //// Q_INVOKABLE void setItemFieldValue(const QString &uuid, const QString &newFieldValue);
    // omitted for the moment

//  //// Q_INVOKABLE void deleteItem(const QString &uuid);
    // also currently not implemented

    void clearModel();
protected:
    //// roleNames is a function that assigns a name to each role
    QHash<int, QByteArray> roleNames() const;

private:
    //// The actual model. Essentially this is a QList of pointers to each Item of the model

    QString modelUuid;
    QList<DeadPerson *> model; //// model is just a list of Items. Item can be a class, object or QSharedPointer, depdending on need
                               // I think this ^ should say list of pointers to Items
    QHash<int, QByteArray> m_roleNames;

    //// // helper functions. Those can be implemented as is, as they perform simple operations like checking the index is valid, etc.
    //// // Item *getItem(const QString &itemUuid);
    //// // int getItemIndex(const QString &itemUuid);
    bool indexValid(const QModelIndex &index) const;
    void notifyDataChanged(int index); // implemented since this is a useful function (see code review)

    // I also haven't implemented all of these

};
} //// end namespace CustomListModel
}  //// end namespace Leapian

#endif // LISTMODELTWO_H
