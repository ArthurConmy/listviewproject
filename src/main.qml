import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

//import Q

Window {
    id: root // I don't think that this is necessary for the
             // list view's height and width but regardless
             // seems a pretty good idea
    visible: true

    width: 640
    height: 700
    title: qsTr("Names and death years")

    ListViewOne { // the bulk of the program
        id: theListView
        height: parent.height
        width: parent.width

        anchors.centerIn: parent // I think this was a requirement in notion; should appear in
    }

    Text { // essentially this part is just copy+pasted over
        id: localTime
        anchors.top: theListView.top
        anchors.right: theListView.horizontalCenter
        anchors.margins: 20

        height: 50
        width: 120
        //// The currentTime will automaticaly be updated as soon as the timeChanged() signal is triggered
        text: time.currentTime
    }
}
