#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QTimer>

#include "listmodeltwo.h"
#include "timerclass.h"

int main(int argc, char *argv[])
{   
    // notion boilerplate:

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext *rootContext = engine.rootContext();

    QString theModelId("exampleModelId");
    //// Create the model Item and pass a reference to the QGuiApplication app. This sets the app to be the parent of the listmodel class
    Leapian::CustomListModel::ListModelTwo model(theModelId, &app);
    //// set the context property. The string that we specify here is how the class
    //// will be "known" in the QML so in this case all we need is to call simpleListModel from the QML
    rootContext->setContextProperty("simpleListModel", &model);

    // also set context for the timer
    Leapian::TimerClass* time = new Leapian::TimerClass(&app);
    rootContext->setContextProperty("time", time);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    // now do the rest of the timer stuff (copy+pasted)

    QTimer *timer = new QTimer(&app);
    timer->setInterval(1000);
    timer->start();
    ////connect the timer with the slot: slotTimeChanged() using a lamda function
    app.connect(timer, &QTimer::timeout, [time](){
        time->slotTimeChanged();});

    // finally launch app

    return app.exec();
}
